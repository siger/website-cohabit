#!/bin/bash
#générateur statique de site web du fablab cohabit
set -o verbose
set -o nounset
LISTE_DES_PROJETS="./tmp/_LISTE_DES_PROJETS"

PAGE_PROJETS_SCIENCES="./projets/fr_science.html"
PAGE_PROJETS_JEUX="./projets/fr_jeux.html"
PAGE_PROJETS_ROBOTS="./projets/fr_robotique.html"
PAGE_PROJETS_OVNIS="./projets/fr_obj-volants.html"
PAGE_PROJETS_ARTS="./projets/fr_arts.html"
PAGE_PROJETS_FABADD="./projets/fr_fab-additive.html"
PAGE_PROJETS_ECOLOGIE="./projets/fr_ecologie.html"

CORPS_PROJETS_SCIENCES="./tmp/_CORPS_PROJETS_SCIENCES"
CORPS_PROJETS_JEUX="./tmp/_CORPS_PROJETS_JEUX"
CORPS_PROJETS_ROBOTS="./tmp/_CORPS_PROJETS_ROBOTS"
CORPS_PROJETS_ARTS="./tmp/_CORPS_PROJETS_ARTS"
CORPS_PROJETS_FABADD="./tmp/_CORPS_PROJETS_FABADD"
CORPS_PROJETS_OVNIS="./tmp/_CORPS_PROJETS_OVNIS"
CORPS_PROJETS_ECOLOGIE="./tmp/_CORPS_PROJETS_ECOLOGIE"

CORPSPROJET="./tmp/_CORPS_PROJET"

MENU="./cons/MENU"
ML_RSS="./actus/ML_RSS"
ENTETE="./cons/ENTETE"
PIEDS="./cons/_FOOTER"
INDEX="./index.html"
ACTUSTRIEES="./tmp/actutriees"
CORPS_RSS="./tmp/corps_rss"
ENTETE_RSS="./cons/entete_rss"
PIED_RSS="./cons/pied_rss"
RSS_FEED="./actus/pgp-actus.xml"
HEADER_fr_PROJETS="./cons/_HEADER_fr_PROJETS"
HEADER_fr_PROJETS_SCIENCES="./cons/_HEADER_fr_PROJETS_SCIENCES"
HEADER_fr_PROJETS_ARTS="./cons/_HEADER_fr_PROJETS_ARTS"
HEADER_fr_PROJETS_OVNIS="./cons/_HEADER_fr_PROJETS_OVNIS"
HEADER_fr_PROJETS_ROBOTS="./cons/_HEADER_fr_PROJETS_ROBOTS"
HEADER_fr_PROJETS_JEUX="./cons/_HEADER_fr_PROJETS_JEUX"
HEADER_fr_PROJETS_ECOLOGIE="./cons/_HEADER_fr_PROJETS_ECOLOGIE"
HEADER_fr_PROJETS_FABADD="./cons/_HEADER_fr_PROJETS_FABADD"


PRE_FOOTER_PROJETS_SCIENCES="./cons/_PRE_FOOTER_PROJETS_SCIENCES"



function checkValues() {
                grep -w "$1" "$2" | cut -d '|' -f 2
        }
function checkMedia() {
                grep -w "$1" "$2" | cut -d '|' -f 1
                }

# on enregistre l’IFS actuel
R=$IFS

# on change l’IFS, pour être un retour à la ligne
IFS='
'
# RAZ des listes tempo
rm $LISTE_DES_PROJETS
rm $CORPS_PROJETS_SCIENCES $CORPS_PROJETS_OVNIS $CORPS_PROJETS_ARTS $CORPS_PROJETS_FABADD $CORPS_PROJETS_ECOLOGIE $CORPS_PROJETS_ROBOTS $CORPS_PROJETS_JEUX
> $CORPS_PROJETS_SCIENCES $CORPS_PROJETS_OVNIS $CORPS_PROJETS_ARTS $CORPS_PROJETS_FABADD $CORPS_PROJETS_ECOLOGIE $CORPS_PROJETS_ROBOTS $CORPS_PROJETS_JEUX

### générateur des pages projet fr et en ###

# générer la liste des Projets
find /opt/redmine/redmine-3.4.4/files -type f -name "*.projet" | sort -V > $LISTE_DES_PROJETS


### générateur des pages projet fr et en ###
cat $LISTE_DES_PROJETS | while read ligne
do
 # Lecture du fichier .projet et enregistrement des variables
 AUTEUR=`checkValues "_AUTEUR" $ligne`
 NOM_DU_PROJET=`checkValues "_NOM_DU_PROJET" $ligne`
 RESUMEE=`checkValues "_RESUMEE" $ligne`
 VIGNETTE=`checkValues "_VIGNETTE" $ligne`
 IMAGE=`checkValues "_IMAGE" $ligne`
 VIDEO=`checkValues "_VIDEO" $ligne`
 SON=`checkValues "_SON" $ligne`
 PRESENTATION=`checkValues "_PRESENTATION" $ligne`
 SOURCES=`checkValues "_SOURCES" $ligne`
 DOCUMENTATION=`checkValues "_DOCUMENTATION" $ligne`
 COMPOSANTS=`checkValues "_COMPOSANTS" $ligne`
 CATEGORIE=`checkValues "_CATEGORIE" $ligne`

 rm -rf ./tmp/grep-categories
 > ./tmp/grep-categories

 echo -n "$CATEGORIE" >> /var/www/html/fablab/tmp/grep-categories
 GREPSCIENCES=`grep -w "sciences" ./tmp/grep-categories`
 GREPARTS=`grep -w "arts" ./tmp/grep-categories`
 GREPOVNIS=`grep -w "ovnis" ./tmp/grep-categories`
 GREPROBOTS=`grep -w "robots" ./tmp/grep-categories`
 GREPFABADD=`grep -w "fabrication_additive" ./tmp/grep-categories`
 GREPECOLOGIE=`grep -w "ecologie" ./tmp/grep-categories`
 GREPJEUX=`grep -w "jeux" ./tmp/grep-categories`


 # crée une balise html, texte avec lien pour chaque page projet étiquetée sciences
 # et les enregistre dans le fichier /cons/_CORPS_PAGE_PROJETS_SCIENCE #
 #echo -n "<a href=\"./""$NOMDUPROJET"".html\"># "$NOMDUPROJET"" : ""$NOMDUPROJET"</a><br>" >> $INDEX_PROJETS
 #echo -n "$CATEGORIE" >> ./tmp/grep-categories
 mkdir ./media/projets/"$NOM_DU_PROJET"
 LIEN_VIGNETTE=`find /opt/redmine/redmine-3.4.4/files -type f -name "*"$VIGNETTE""`
#test provisoire
 echo "$LIEN_VIGNETTE" >> ./tmp/_test_lien_vignette
# copie
 cp "$LIEN_VIGNETTE" ./media/projets/"$NOM_DU_PROJET"/"$VIGNETTE"

# conversion de l'image en 300x300 px sans déformation
convert -resize 300 ./media/projets/"$NOM_DU_PROJET"/"$VIGNETTE" ./media/projets/"$NOM_DU_PROJET"/resize-"$VIGNETTE"
convert -background white -gravity center -crop 300x300+0+0 ./media/projets/"$NOM_DU_PROJET"/resize-"$VIGNETTE" ./media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"


    if [ "$GREPSCIENCES" = "sciences" ]
      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS_SCIENCES
    fi
    if [ "$GREPOVNIS" = "ovnis" ]
      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS_OVNIS
    fi
    if [ "$GREPJEUX" = "jeux" ]
      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS_JEUX
    fi
    if [ "$GREPROBOTS" = "robots" ]
      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS_ROBOTS
    fi
    if [ "$GREPARTS" = "arts" ]
      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS_ARTS
    fi
    if [ "$GREPFABADD" = "fabrication_additive" ]
      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS_FABADD
    fi
    if [ "$GREPECOLOGIE" = "ecologie" ]
      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS_ECOLOGIE
    fi
    # génération de la page projet
#    if [ "$GREPVID" = "video" ]
#      then echo "<article id=\"v1\" class=\"vignette\"><a href=\"""$NOM_DU_PROJET"".html\"><img width=\"200\" src=\"""https://projets.cohabit.fr/fablab/media/projets/"$NOM_DU_PROJET"/vignette-"$VIGNETTE"""\" alt=\"""$NOM_DU_PROJET"\"" class=\"vignette-image\"/><h2 class=\"vignette-titre vig-titre-projet\">""$NOM_DU_PROJET""</h2><div class=\"vignette-text\"><div class=\"text vig-text-projet\">""$RESUMEE""</div></div></a></article>" >> $CORPS_PROJETS
#    fi
done
# génération de la page des projets sciences #
cat $HEADER_fr_PROJETS $HEADER_fr_PROJETS_SCIENCES $CORPS_PROJETS_SCIENCES $PRE_FOOTER_PROJETS_SCIENCES $PIEDS > $PAGE_PROJETS_SCIENCES
# génération de la page des projets jeux #
cat $HEADER_fr_PROJETS $HEADER_fr_PROJETS_JEUX $CORPS_PROJETS_JEUX $PRE_FOOTER_PROJETS_SCIENCES $PIEDS > $PAGE_PROJETS_JEUX
# concaténation des éléments de la page des projets arts #
cat $HEADER_fr_PROJETS $HEADER_fr_PROJETS_ARTS $CORPS_PROJETS_ARTS $PRE_FOOTER_PROJETS_SCIENCES $PIEDS > $PAGE_PROJETS_ARTS
# concaténation des éléments de la page des projets fabrication additivTITREe #
cat $HEADER_fr_PROJETS $HEADER_fr_PROJETS_FABADD $CORPS_PROJETS_FABADD $PRE_FOOTER_PROJETS_SCIENCES $PIEDS > $PAGE_PROJETS_FABADD
# concaténation des éléments de la page des projets objets volants #
cat $HEADER_fr_PROJETS $HEADER_fr_PROJETS_OVNIS $CORPS_PROJETS_OVNIS $PRE_FOOTER_PROJETS_SCIENCES $PIEDS > $PAGE_PROJETS_OVNIS
# concaténation des éléments de la page des projets agriculture-écologie #
cat $HEADER_fr_PROJETS $HEADER_fr_PROJETS_ECOLOGIE $CORPS_PROJETS_ECOLOGIE $PRE_FOOTER_PROJETS_SCIENCES $PIEDS > $PAGE_PROJETS_ECOLOGIE
# concaténation des éléments de la page des projets robots #
cat $HEADER_fr_PROJETS $HEADER_fr_PROJETS_ROBOTS $CORPS_PROJETS_ROBOTS $PRE_FOOTER_PROJETS_SCIENCES $PIEDS > $PAGE_PROJETS_ROBOTS


### génération de chaque page projet ###
# cat $PROJETS | while read ligneTITRE
# do
#     # Lecture du fichier .projet
#     AUTEUR=`checkValues "_auteur" $ligne`
#     TITRE=`checkValues "_titre" $ligne`
#     VIGNETTE=`checkValues "_vignette" $ligne`
#     VIDEO=`checkValues "_video" $ligne`
#     SON=`checkValues "_son" $ligne`
#     IMAGE=`checkValues "_image" $ligne`
#     RESUMEE=`checkValues "_resumee" $ligne`
#     SYNOPSIS=`checkValues "_synopsis" $ligne`
#     SOURCES=`checkValues "_sources" $ligne`
#     DOCUMENTATION=`checkValues "_documentation" $ligne`
#     TUTORIEL=`checkValues "_tutoriel" $ligne`
#     COMPOSANTS=`checkValues "_composants" $ligne`
#     CATEGORIE=`grep -w "_video" src="https://projets.cohabit.fr/fablab/tmp/ | cut -d '|' -f 1` ;
#     SLIGNE=$ligne
#     echo "$SLIGNE" > src="https://projets.cohabit.fr/fablab/tmp/temporaryln
#
#     # écriture du titre et du texte
#     echo -n "<div>
#     <a class=\"green\">NOŒUD# ""$NUMNOEUD"" : ""$TITRE""</a>
#     <p class=\"cell-p\">""$TEXTE""</p>
#     </div>" >> $CORPSNOEUD
#
# ## écriture de la liste divs media
#     cat $SLIGNE | while read line
#     do
#       echo "$line" > src="https://projets.cohabit.fr/fablab/tmp/temporarycat ;
#       GREPVID=`grep -w "_video" src="https://projets.cohabit.fr/fablab/tmp/temporarycat | cut -d '|||' -f 1` ;
#       GREPIMG=`grep -w "_img" src="https://projets.cohabit.fr/fablab/tmp/temporarycat | cut -d '|||' -f 1` ;
#       GREPCOMMENTAIRE=`grep -w "_commentaire" src="https://projets.cohabit.fr/fablab/tmp/temporarycat | cut -d '|||' -f 1` ;
#       GREPSON=`grep -w "_son" src="https://projets.cohabit.fr/fablab/tmp/temporarycat | cut -d '|||' -f 1` ;
#
#       if [ "$GREPVID" = "_video" ]
#         then VIDEO=`cut -d '|||' -f 2 src="https://projets.cohabit.fr/fablab/tmp/temporarycat` ;
#         echo -n "<div>
#         <iframe width=\"560\" height=\"315\" sandbox=\"allow-same-origin allow-scripts\" src=\"""$VIDEO""\" frameborder=\"0\" allowfullscreen></iframe>
#         </div>
#         " >> $CORPSNOEUD
#       elif [ "$GREPIMG" = "_img" ]
#         then IMG=`cut -d '|||' -f 2 src="https://projets.cohabit.fr/fablab/tmp/temporarycat` ;
#         echo -n "<div class="gallery">
#         <a href=\"src="https://projets.cohabit.fr/fablab/img/""$NUMNOEUD""/""$IMG""\" ><img class=media src=\"src="https://projets.cohabit.fr/fablab/img/""$NUMNOEUD""/""$IMG""\" alt=\"""$TITRE""\" width=\"80%\" /></a>
#         </div>
#         " >> $CORPSNOEUD
# # tentative de créer automatiquement le lien image du dossier Noeuds dans le repertoire /img du siteweb
#         DOSSIMG=`cut -d'/' -f1-6 src="https://projets.cohabit.fr/fablab/tmp/temporaryln`
#         mkdir src="https://projets.cohabit.fr/fablab/img/"$NUMNOEUD" # création du dossier image pour le nœud
#         ln "$DOSSIMG"/"$IMG" src="https://projets.cohabit.fr/fablab/img/"$NUMNOEUD"/"$IMG"
#
#       elif [ "$GREPCOMMENTAIRE" = "_commentaire" ]
#         then COMMENTAIRE=`cut -d '|||' -f 2 src="https://projets.cohabit.fr/fablab/tmp/temporarycat` ;
#         echo -n "<div>
#         <p class=\"cell-p\">""$COMMENTAIRE""</p>
#         </div>
#         " >> $CORPSNOEUD
#       elif [ "$GREPSON" = "_son" ]
#         then SON=`cut -d '|||' -f 2 src="https://projets.cohabit.fr/fablab/tmp/temporarycat` ;
#         echo -n "<div><audio controls><source src=\"src="https://projets.cohabit.fr/fablab/son/""$NUMNOEUD""/""$SON""\" type="audio/ogg">
#         Your browser does not support the audio element.
#         </audio></div>
#         " >> $CORPSNOEUD
# # tentative de créer automatiquement le lien son du dossier Noeuds dans le repertoire /son du siteweb
#         DOSSON=`cut -d'/' -f1-6 src="https://projets.cohabit.fr/fablab/tmp/temporaryln`
#         mkdir src="https://projets.cohabit.fr/fablab/son/"$NUMNOEUD" # création du dossier son pour le nœud
#         ln "$DOSSON"/"$SON" src="https://projets.cohabit.fr/fablab/son/"$NUMNOEUD"/"$SON"
#
#       else echo "rien" /dev/null
#       fi
#
#     done
#
#     # concatenation de la page noeud
# cat $HEADER_fr_PROJET $CORPS_PROJET $PIEDS > $NOMDUPROJET.html
#
#     # raz du fichier temporaire des divs de la page noeud
#     rm "$CORPSNOEUD"
# done
#
# ### génération de la page actus.html ###
# # RAZ liste des actus
# rm $CORPSACTUS
# # RAZ corps RSS_FEED
# rm $CORPS_RSS
# # générer la liste des actualités
# find src="https://projets.cohabit.fr/fablab/actus/ -type f -name "*.actuconf" > $ACTUS
# sort -n -r $ACTUS > $ACTUSTRIEES
#         cat $ACTUSTRIEES | while read lignex
#         do
#         # Lecture du fichier actus
#         EVENEMENT=`checkValues "_evenement" $lignex`
#         DATE=`checkValues "_date" $lignex`
#         AFFICHE=`checkValues "_affiche" $lignex`
#         PITCH=`checkValues "_pitch" $lignex`
#         LIEN_EVENEMENT=`checkValues "_lien" $lignex`
#         LIEU=`checkValues "_lieu" $lignex`
#
#
#         echo -n "<div class=gallery>
#         <a href=\"$LIEN_EVENEMENT\" target=\"_blank\">
#         <img class=media src=\"src="https://projets.cohabit.fr/fablab/img/actus/""$AFFICHE""\" alt=\""affiche"\" width=\"200px\" /></a>
#         <a class=\"cell_titre\" href=\"$LIEN_EVENEMENT\">""$EVENEMENT""</a>
#         <a class=\"cell-p\" href=\"$LIEN_EVENEMENT\">""$DATE""</a>
#         <p class=\"cell-p\">""$PITCH""</p>
#         <p class=\"cell-p\">Lieu: ""$LIEU""</p>
#         </div>
#         " >> $CORPSACTUS
#
#         echo -n "<item>
#             <title>""$EVENEMENT""</title>
#             <image><url>""$AFFICHE""</url><link>""$LIEN_EVENEMENT""</link></image>
#             <description>""$DATE""<br>""$PITCH""</description>
#             <link>$LIEN_EVENEMENT</link>
#         </item>" >> $CORPS_RSS
#
#         done
#         cat $ENTETE $ENTETE_CORPSNOEUD $CORPSACTUS $ENTETE_INDEX $INDEX_PROJETS $PIEDS > src="https://projets.cohabit.fr/fablab/actus.html
#
# #génération about.html
# cat $ENTETE  $MENU_ABOUT $CORPSABOUT $PIEDS > src="https://projets.cohabit.fr/fablab/about.html
#
# #génération du fichier rss/xml
# cat $ENTETE_RSS $CORPS_RSS $PIED_RSS > $RSS_FEED
#
# #on rétablit l’IFS
# IFS=$R
#
# #synchronisation avec l'hébergement
# #rsync -avz -e "ssh -i /home/pgp/.ssh/id_rsa -p 55555" /media/pgp/disque2/Websites/pgp-web-static-cli/* root@web.openbeelab.org:/var/www/pgp/
#
# exit 0
